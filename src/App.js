import {BrowserRouter, Route, Routes} from "react-router-dom";
import Header from "./layouts/Header";
import Home from "./pages/home";
import NotFound404 from "./pages/404";
import PostOnClick from "./components/PostOnClick";
import PostPageLoad from "./components/PostPageLoad";
import Login from "./pages/login";
import Cabinet from "./pages/cabinet";
import Test from "./pages/test";

const App = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Test/>}/>
                {/*<Route path="/" element={<Home/>}/>*/}
                <Route path="/post" element={<PostOnClick/>}/>
                <Route path="/post2" element={<PostPageLoad/>}/>
                <Route path="/login" element={<Login/>}/>
                <Route path="/cabinet" element={<Cabinet/>}/>
                <Route path="*" element={<NotFound404/>}/>
            </Routes>
        </BrowserRouter>
    )
        ;
}

export default App;

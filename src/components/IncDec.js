import {useState} from "react";

const IncDec = () => {
    const [num, setNum] = useState(0);

    // const inc = () => setNum(num + 1);

    const dec = () => setNum(num - 1)

    return (
        <div>
            <button onClick={dec}>-</button>
            <h1 id="qalay">{num}</h1>
            <button onClick={() => setNum(num + 1)}>+</button>
        </div>
    );
}

export default IncDec;
import axios from "axios";
import {useEffect, useState} from "react";
import {Table} from "reactstrap";

const MyQueueByService = () => {

    useEffect(() => {
            getMyQueueByService();
        },
        []);

    const [dates, setDates] = useState([]);
    const [services, setServices] = useState([]);

    const getMyQueueByService = () => {
        axios.get(
            'http://localhost:80/dashboard/my-by-service?period=MONTHLY&startDate=2023-10-01',
            {
                auth: {
                    username: "Akmal",
                    password: "123"
                }
            })
            .then(res => {
                let data = res.data.data;
                setDates(data.dates);
                setServices(data.services);
            })
            .catch(err => {
                console.log(err);
            })
    }

    console.log(dates)
    console.log(services)

    return (
        <div>
            <Table hover>
                <thead>
                <tr>
                    <th>
                        TR
                    </th>
                    <th>
                        Service name
                    </th>
                    {dates.map((item, i) =>
                        <th key={Math.random()}>
                            {item.date}
                        </th>
                    )}
                </tr>
                </thead>
                <tbody>
                {services.map((item, i) =>
                    <tr key={Math.random()}>
                        <td>{i + 1}</td>
                        <td>{item.name}</td>
                        {dates.map((el, j) =>
                            <td key={Math.random()}>{el.values[item.id] ?? 0}</td>
                        )}
                    </tr>
                )}
                </tbody>
            </Table>
        </div>
    )
}
export default MyQueueByService;
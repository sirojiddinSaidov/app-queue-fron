import {useState} from "react";
import axios from "axios";
import {Button, Table} from "reactstrap";

const PostOnClick = () => {
    const [posts, setPosts] = useState([]);

    const bor = () => {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(res => {
                console.log(res.data);
                setPosts(res.data)
            })
    }
    return (
        <div>
            {/*<IncDec/>*/}

            <Button onClick={bor}>Borib kel</Button>
            <Table hover>
                <thead>
                <tr>
                    <th>
                        #
                    </th>
                    <th>
                        UserId
                    </th>
                    <th>
                        Id
                    </th>
                    <th>
                        Title
                    </th>
                    <th>
                        Body
                    </th>
                </tr>
                </thead>
                <tbody>
                {posts.map((post, i) =>
                    <tr>
                        <th scope="row">
                            {i + 1}
                        </th>
                        <td>
                            {post.userId}
                        </td>
                        <td>
                            {post.id}
                        </td>
                        <td>
                            {post.title}
                        </td>
                        <td>
                            {post.body}
                        </td>
                    </tr>
                )}
                </tbody>

            </Table>
        </div>
    );
}

export default PostOnClick;
import {useEffect} from "react";
import {useNavigate} from "react-router-dom";
import {Button} from "reactstrap";
import axios from "axios";

const Cabinet = () => {
    const navigate = useNavigate();
    useEffect(() => {
        if (!localStorage.getItem("Token")) {
            navigate('/login')
        } else {
            axios.get('http://localhost:8080/api/service',
                {
                    headers: {
                        Authorization: localStorage.getItem("Token")
                    }
                })
        }
    }, [])
    const logout = () => {
        localStorage.removeItem("Token");
        navigate('/login')
    }

    return (
        <div>
            <Button onClick={logout}>Logout</Button>
            <h1>Cabinet page</h1>
        </div>
    )
}
export default Cabinet;
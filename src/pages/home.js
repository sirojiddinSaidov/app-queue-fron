import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText,
    Container,
    Row,
    Col,
    Modal,
    ModalHeader,
    ModalFooter,
    Button,
    ModalBody,
    Card,
    CardBody,
    CardTitle,
    CardSubtitle, CardText
} from "reactstrap";
import {useEffect, useState} from "react";
import axios from "axios";

const Home = () => {
    const [isOpen, setIsOpen] = useState(false);
    const [levels, setLevels] = useState([]);
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const [serviceId, setServiceId] = useState();
    const [checkModalIsOpen, setCheckModalIsOpen] = useState(false);
    const [check, setCheck] = useState({});

    const getServices = () => {
        axios.get('http://localhost/service/for-queue')
            .then(res => setLevels(res.data.data))
            .catch(err => console.log(err))
    }

    useEffect(() => {
        getServices()
    }, [])

    const toggle = () => setIsOpen(!isOpen);

    const modalniOchYokiYop = (serviceId) => {
        setServiceId(serviceId)
        setModalIsOpen(!modalIsOpen)
    }

    const takeQueue = () => {
        axios.post('http://localhost/queue/add',
            {"serviceId": serviceId})
            .then(res => {
                setCheck(res.data.data)
                modalniOchYokiYop();
                setCheckModalIsOpen(true)
            })
            .catch(err => console.log(err))
    }


    return (
        <div>
            <Navbar>
                <NavbarBrand href="/">reactstrap</NavbarBrand>
                <NavbarToggler onClick={toggle}/>
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="me-auto" navbar>
                        <NavItem>
                            <NavLink href="/components/">Components</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://github.com/reactstrap/reactstrap">
                                GitHub
                            </NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                Options
                            </DropdownToggle>
                            <DropdownMenu>
                                <DropdownItem>Option 1</DropdownItem>
                                <DropdownItem>Option 2</DropdownItem>
                                <DropdownItem divider/>
                                <DropdownItem>Reset</DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                    <NavbarText>Simple Text</NavbarText>
                </Collapse>
            </Navbar>

            <Container>

                <Row>
                    {levels.map((item, i) =>
                        <Col key={item.level} className="bg-light border">
                            {item.level}
                            {item.services.map((service, j) =>
                                <Row
                                    onClick={() => modalniOchYokiYop(service.id)}
                                    key={service.id} className="mt-3"
                                    style={{cursor: 'pointer'}}>
                                    <Col className="bg-light border">
                                        {service.name}
                                    </Col>
                                </Row>
                            )}
                        </Col>
                    )}
                </Row>
            </Container>

            <Modal isOpen={modalIsOpen} toggle={modalniOchYokiYop}>
                <ModalHeader>
                    Rostdan ham navbat olmoqchimisiz?
                </ModalHeader>

                <ModalFooter>
                    <Button color="danger" onClick={modalniOchYokiYop}>Bekor qilish</Button>
                    <Button color="success" onClick={takeQueue}>Tasdiqlash</Button>
                </ModalFooter>
            </Modal>

            <Modal isOpen={checkModalIsOpen}>
                <ModalHeader>
                    SIzning checkingiz
                </ModalHeader>

                <ModalBody>
                    <Card>
                        <CardBody>
                            <h1>Sizdan oldin: <span>{check.beforeCount}</span></h1>
                            <h1>Check olingan vaqt: <span>{check.createdAt}</span></h1>
                            <h1>Check raqami: <span>{check.number}</span></h1>
                            <h1>Taxminiy vaqt: <span>{check.roundTime}</span></h1>
                            <h1>Xizmat nomi: <span>{check.serviceName}</span></h1>
                            <Button onClick={() => setCheckModalIsOpen(false)}>
                                Yopish
                            </Button>
                        </CardBody>
                    </Card>
                </ModalBody>
            </Modal>
        </div>
    )
}
export default Home;
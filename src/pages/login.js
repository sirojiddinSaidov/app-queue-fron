import {Button, Input} from "reactstrap";
import axios from "axios";
import {useState} from "react";
import {useNavigate} from "react-router-dom";

const Login = () => {
    const [user, setUser] = useState({});

    const navigate = useNavigate();


    const login = () => {
        axios.post('http://localhost:8080/api/auth/login', user)
            .then(res => {
                localStorage.setItem("Token", 'Bearer ' + res.data.data);
                navigate("/cabinet")
            }).catch(err => {
            console.log(err);
        })
    }

    const onChange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value,
        });
    }


    return (
        <div>
            <h1>Login page</h1>
            <Input onChange={onChange}
                   name="username"/>
            <Input onChange={onChange}
                   type="password"
                   name="password"/>
            <Button onClick={login}>Login</Button>
        </div>
    )
}

export default Login;
import {useEffect} from "react";
import axios from "axios";

const Service = () => {

    useEffect(() => {
        axios.get('http://localhost:8080/api/service',
            {
                headers: {
                    Authorization: localStorage.getItem("Token")
                }
            })
    }, [])

    return (
        <div>
            <h1>Service page</h1>
        </div>
    )
}

export default Service;